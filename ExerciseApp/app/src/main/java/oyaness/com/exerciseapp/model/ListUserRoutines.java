package oyaness.com.exerciseapp.model;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Oya on 17.12.2016.
 */

public class ListUserRoutines {
    private ArrayList<UserRoutine> listOfUserRoutine;

    public ListUserRoutines() {
    }

    public ArrayList<UserRoutine> getListOfUserRoutine() {
        return listOfUserRoutine;
    }

    public void setListOfUserRoutine(ArrayList<UserRoutine> listOfUserRoutine) {
        this.listOfUserRoutine = listOfUserRoutine;
    }

    public UserRoutine getRoutineWithId(long id) {
        for (UserRoutine routine : listOfUserRoutine) {
            if (routine.getRoutine().getId() == id) {
                return routine;
            }
        }
        return null;
    }

    public int removeUserRoutineWithId(long id) {
        Iterator<UserRoutine> iterator = listOfUserRoutine.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            UserRoutine userRoutine = iterator.next();
            if(userRoutine.getRoutine().getId()==id){
                iterator.remove();
                return i;
            }
            i++;
        }
        return -1;
    }
}
