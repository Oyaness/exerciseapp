package oyaness.com.exerciseapp.adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import oyaness.com.exerciseapp.R;
import oyaness.com.exerciseapp.activities.RoutineActivity;
import oyaness.com.exerciseapp.model.UserRoutine;
import oyaness.com.exerciseapp.utils.Constants;
import oyaness.com.exerciseapp.utils.RemoveRoutineListener;

/**
 * Created by Oya on 17.12.2016.
 */

public class RoutineAdapter extends  RecyclerView.Adapter<RoutineAdapter.ViewHolder> {

    private RemoveRoutineListener listener;
    private ArrayList<UserRoutine> routines;
    private Activity context;
    public RoutineAdapter(Activity context, ArrayList<UserRoutine> routines, RemoveRoutineListener listener) {
        this.routines = routines;
        this.context = context;
        this.listener = listener;
    }

    public ArrayList<UserRoutine> getRoutines() {
        return routines;
    }

    public void setRoutines(ArrayList<UserRoutine> routines) {
        this.routines = routines;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_routine, parent, false));
    }

    @Override
    public void onBindViewHolder(RoutineAdapter.ViewHolder holder, int position) {
        UserRoutine routine = routines.get(position);
        holder.update(routine);
    }

    @Override
    public int getItemCount() {
        return routines.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView routineTitle;
        TextView routineDuration;
        TextView routineGoal;
        TextView routineExercises;
        ImageView routineImage;
        UserRoutine routine;

        ViewHolder(View itemView) {
            super(itemView);

            routineTitle = (TextView) itemView.findViewById(R.id.tv_title);
            routineDuration = (TextView) itemView.findViewById(R.id.tv_duration);
            routineGoal = (TextView) itemView.findViewById(R.id.tv_goal);
            routineExercises = (TextView) itemView.findViewById(R.id.tv_exerices);
            routineImage = (ImageView) itemView.findViewById(R.id.iv_routine_image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, RoutineActivity.class);
                    intent.putExtra(Constants.USER_ROUTINE_EXTRA, routine.getRoutine().getId());
                    context.startActivityForResult(intent, Constants.ROUTINE_INTENT_CODE);
                }
            });
            Button removeButton = (Button) itemView.findViewById(R.id.btn_remove);
            removeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(context)
                            .setMessage(context.getString(R.string.delete_text))
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    listener.deleteRoutine(routine.getRoutine().getId());
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .show();
                }
            });
        }
        void update(UserRoutine routine) {
            this.routine = routine;
            routineTitle.setText(routine.getRoutine().getTitle());
            routineDuration.setText(routine.getRoutine().getDuration());
            routineGoal.setText(routine.getRoutine().getGoal());
            routineGoal.setPaintFlags(routineGoal.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);

            String exercisesString = routine.getRoutine().calculateDoneExercises()+"/"+routine.getRoutine().getSizeExercise();
            routineExercises.setText(exercisesString);

            Picasso.with(context)
                    .load(routine.getRoutine().getImageUrl())
                    .placeholder(R.drawable.placeholder)
                    .resize(400,400)
                    .into(routineImage);
        }
    }
}
