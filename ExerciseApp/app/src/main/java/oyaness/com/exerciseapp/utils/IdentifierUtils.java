package oyaness.com.exerciseapp.utils;

import android.content.Context;

/**
 * Created by Oya on 17.12.2016.
 */

public class IdentifierUtils {

    public static int getResourceId(Context c, String pVariableName, String pResourcename, String pPackageName)
    {
        try {
            return c.getResources().getIdentifier(pVariableName, pResourcename, pPackageName);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
