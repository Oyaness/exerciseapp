package oyaness.com.exerciseapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;
import oyaness.com.exerciseapp.R;
import oyaness.com.exerciseapp.model.Equipment;
import oyaness.com.exerciseapp.model.Routine;
import oyaness.com.exerciseapp.model.RoutineExercise;
import oyaness.com.exerciseapp.utils.Constants;
import oyaness.com.exerciseapp.utils.RoutinesSingleton;

/**
 * Created by Oya on 17.12.2016.
 */

public class ExerciseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int HEADER = 1;
    private static final int EXERCISE = 2;

    private Routine routine;
    private Activity context;

    public ExerciseAdapter(Routine routine, Activity context) {
        this.routine = routine;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER;
        } else {
            return EXERCISE;
        }
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder holder = null;
        switch (viewType) {
            case HEADER:
                view = LayoutInflater.from(context).inflate(R.layout.list_item_header, null);
                holder = new ExerciseAdapter.HeaderViewHolder(view);
                break;
            case EXERCISE:
                view = LayoutInflater.from(context).inflate(R.layout.list_item_exercise, null);
                holder = new ExerciseAdapter.ExerciseViewHolder(view);
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position == 0) {
            ExerciseAdapter.HeaderViewHolder viewHolder = (ExerciseAdapter.HeaderViewHolder) holder;
            viewHolder.update();

        } else {
            ExerciseAdapter.ExerciseViewHolder viewHolder = (ExerciseAdapter.ExerciseViewHolder) holder;
            viewHolder.update(routine.getRoutineExercises().get(position-1));
        }
    }

    @Override
    public int getItemCount() {
        if (routine.getRoutineExercises()!= null && routine.getRoutineExercises().size()>0){
            return routine.getRoutineExercises().size()+1;
        }
        return 1;
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        private TextView detailsTitle;
        private TextView routineTitle;
        private TextView routineDuration;
        private TextView routineGoal;
        private TextView routineBodyFocus;
        private TextView routineFitnessLvl;
        private TextView routineIntensity;
        private ImageView routineImage;
        private LinearLayout equipmentsView;
        private RecyclerView rvEquipment;
        private TextView routineDescription;
        private TextView routineDetails;
        private Button removeButton;

        HeaderViewHolder(View itemView) {
            super(itemView);
            routineTitle = (TextView) itemView.findViewById(R.id.tv_title);
            routineDuration = (TextView) itemView.findViewById(R.id.tv_duration);
            routineGoal = (TextView) itemView.findViewById(R.id.tv_goal);
            routineBodyFocus = (TextView) itemView.findViewById(R.id.tv_body_focus);
            routineFitnessLvl = (TextView) itemView.findViewById(R.id.tv_fitness_lvl);
            routineIntensity = (TextView) itemView.findViewById(R.id.tv_intensity);
            routineImage = (ImageView) itemView.findViewById(R.id.iv_routine_image);
            equipmentsView = (LinearLayout) itemView.findViewById(R.id.equipment_layout);
            rvEquipment = (RecyclerView) itemView.findViewById(R.id.rv_equipment);
            routineDescription = (TextView) itemView.findViewById(R.id.tv_description);
            routineDetails = (TextView) itemView.findViewById(R.id.tv_details);
            removeButton = (Button) itemView.findViewById(R.id.btn_remove);
            detailsTitle = (TextView) itemView.findViewById(R.id.tv_details_title);
            removeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(context)
                            .setMessage(context.getString(R.string.delete_text))
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent returnIntent = new Intent();
                                    returnIntent.putExtra(Constants.USER_ROUTINE_EXTRA ,routine.getId());
                                    context.setResult(Constants.ROUTINE_INTENT_CODE, returnIntent);
                                    context.finish();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .show();
                }
            });
        }

        void update() {
            routineTitle.setText(routine.getTitle());
            routineDuration.setText(routine.getDuration());
            routineGoal.setText(routine.getGoal());
            routineGoal.setPaintFlags(routineGoal.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
            routineBodyFocus.setText(routine.getFocusArea());
            routineFitnessLvl.setText(routine.getFitnessLevel());
            routineIntensity.setText(routine.getIntensity());
            routineDescription.setText(routine.getDescription());
            routineDetails.setText(routine.getImprovementInfo());
            if (routine.getImprovementInfo() ==null){
                detailsTitle.setVisibility(View.GONE);
                routineDetails.setVisibility(View.GONE);
            }


            Picasso.with(context)
                    .load(routine.getImageUrl())
                    .placeholder(R.drawable.placeholder)
                    .resize(400,400)
                    .into(routineImage);

            rvEquipment.setLayoutManager( new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            ArrayList<Equipment> equipments = routine.getEquipmentsForRoutine();
            if (equipments != null &&equipments.size()>0) {
                EquipmentAdapter adapter = new EquipmentAdapter(equipments, context);
                rvEquipment.setAdapter(adapter);
            }
            else {
                equipmentsView.setVisibility(View.GONE);
            }
        }
    }

    private class ExerciseViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView exerciseImage;
        private TextView exerciseName;
        private TextView exerciseBreaks;
        private TextView exerciseReps;

        ExerciseViewHolder(View itemView) {
            super(itemView);
            exerciseImage = (CircleImageView) itemView.findViewById(R.id.iv_exercise_image);
            exerciseName = (TextView) itemView.findViewById(R.id.tv_exercise_name);
            exerciseBreaks = (TextView) itemView.findViewById(R.id.tv_breaks);
            exerciseReps = (TextView) itemView.findViewById(R.id.tv_reps);
        }

        void update(RoutineExercise exercise) {
            exerciseName.setText(exercise.getExercise().getName());
            if (exercise.getRoutineExercisesSet() != null && exercise.getRoutineExercisesSet().size()>0) {
                SimpleDateFormat df = new SimpleDateFormat("mm:ss");
                Calendar cal = exercise.getRoutineExercisesSet().get(0).getCalendarFromBreaksSec();
                String formattedDate = df.format(cal.getTime());
                exerciseBreaks.setText(formattedDate);
                exerciseReps.setText(Integer.toString(exercise.getRoutineExercisesSet().get(0).getRepeats()));
            }

            Picasso.with(context)
                    .load(exercise.getExercise().getImageUrl())
                    .placeholder(R.drawable.placeholder)
                    .resize(400,400)
                    .into(exerciseImage);
        }
    }
}