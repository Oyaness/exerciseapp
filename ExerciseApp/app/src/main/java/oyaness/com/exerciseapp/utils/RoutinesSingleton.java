package oyaness.com.exerciseapp.utils;

import oyaness.com.exerciseapp.model.ListUserRoutines;

/**
 * Created by Oya on 17.12.2016.
 */

public class RoutinesSingleton {
    private static RoutinesSingleton instance;
    private ListUserRoutines routines;

    private RoutinesSingleton() {

    }

    public static synchronized RoutinesSingleton getInstance() {
        if (instance == null) {
            instance = new RoutinesSingleton();
        }
        return instance;
    }

    public ListUserRoutines getRoutines() {
        return routines;
    }

    public void setRoutines(ListUserRoutines routines) {
        this.routines = routines;
    }
}
