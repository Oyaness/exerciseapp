package oyaness.com.exerciseapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import oyaness.com.exerciseapp.R;
import oyaness.com.exerciseapp.adapters.ExerciseAdapter;
import oyaness.com.exerciseapp.model.UserRoutine;
import oyaness.com.exerciseapp.utils.Constants;
import oyaness.com.exerciseapp.utils.RoutinesSingleton;

/**
 * Created by Oya on 17.12.2016.
 */

public class RoutineActivity extends AppCompatActivity {
    private UserRoutine routine;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routine);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_routine);
        Intent intent = getIntent();
        if(intent.getExtras() != null) {
            long id = intent.getLongExtra(Constants.USER_ROUTINE_EXTRA, 0);
            routine = RoutinesSingleton.getInstance().getRoutines().getRoutineWithId(id);
        }
        if (routine != null && routine.getRoutine()!= null) {
            ExerciseAdapter adapter = new ExerciseAdapter(routine.getRoutine(), this);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_OK);
        super.onBackPressed();
    }
}
