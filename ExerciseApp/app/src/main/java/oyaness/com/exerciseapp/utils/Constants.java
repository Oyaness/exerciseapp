package oyaness.com.exerciseapp.utils;

/**
 * Created by Oya on 17.12.2016.
 */

public class Constants {
    public static final String USER_ROUTINE_EXTRA = "USER_ROUTINE_EXTRA";
    public static final int ROUTINE_INTENT_CODE = 2;
}
