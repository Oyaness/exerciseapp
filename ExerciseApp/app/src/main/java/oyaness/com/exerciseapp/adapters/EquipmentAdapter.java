package oyaness.com.exerciseapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import oyaness.com.exerciseapp.R;
import oyaness.com.exerciseapp.model.Equipment;
import oyaness.com.exerciseapp.utils.IdentifierUtils;

/**
 * Created by Oya on 17.12.2016.
 */

class EquipmentAdapter extends RecyclerView.Adapter<EquipmentAdapter.ViewHolder> {

    private ArrayList<Equipment> equipments;
    private Context context;

    EquipmentAdapter(ArrayList<Equipment> equipments, Context context) {
        this.equipments = equipments;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_equipment, parent, false));
    }

    @Override
    public void onBindViewHolder(EquipmentAdapter.ViewHolder holder, int position) {
        Equipment equipment = equipments.get(position);
        holder.update(equipment);
    }

    @Override
    public int getItemCount() {
        return equipments.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView equipmentName;

        ViewHolder(View itemView) {
            super(itemView);
            equipmentName = (TextView) itemView.findViewById(R.id.tv_equipment);
        }

        void update(Equipment equipment) {
            equipmentName.setText(equipment.getEquipmentName());
            String resourceName = "equipment_"+equipment.getEquipmentId();
            int imageNameId = IdentifierUtils.getResourceId(context, resourceName, "string", context.getPackageName());
            String drawableName = context.getResources().getString(imageNameId);
            int imageDrawableId = IdentifierUtils.getResourceId(context, drawableName, "drawable", context.getPackageName());
            equipmentName.setCompoundDrawablesWithIntrinsicBounds(imageDrawableId,0, 0, 0);
        }
    }
}

