package oyaness.com.exerciseapp.model;


/**
 * Created by Oya on 15.12.2016.
 */

public class UserRoutine {

    private Routine routine;
    private String startDate;
    private long userId;

    public UserRoutine() {
    }

    public Routine getRoutine() {
        return routine;
    }

    public void setRoutine(Routine routine) {
        this.routine = routine;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

}
