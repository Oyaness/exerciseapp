package oyaness.com.exerciseapp.model;

/**
 * Created by Oya on 15.12.2016.
 */

public class Equipment {
    private String equipmentDescription;
    private int equipmentId;
    //TODO is it?
    private String equipmentName;

    public Equipment() {
    }

    public String getEquipmentDescription() {
        return equipmentDescription;
    }

    public void setEquipmentDescription(String equipmentDescription) {
        this.equipmentDescription = equipmentDescription;
    }

    public int getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(int equipmentId) {
        this.equipmentId = equipmentId;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

}
