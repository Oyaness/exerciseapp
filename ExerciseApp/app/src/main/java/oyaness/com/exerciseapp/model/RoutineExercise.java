package oyaness.com.exerciseapp.model;


import java.util.ArrayList;

/**
 * Created by Oya on 15.12.2016.
 */

public class RoutineExercise {
    private Boolean done;
    private Exercise exercise;
    private int orderNo;
    private long routineExerciseId;
    private ArrayList<RoutineExerciseSet> routineExercisesSet;

    public RoutineExercise() {
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public Exercise getExercise() {
        return exercise;
    }

    public void setExercise(Exercise exercise) {
        this.exercise = exercise;
    }

    public int getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(int orderNo) {
        this.orderNo = orderNo;
    }

    public long getRoutineExerciseId() {
        return routineExerciseId;
    }

    public void setRoutineExerciseId(long routineExerciseId) {
        this.routineExerciseId = routineExerciseId;
    }

    public ArrayList<RoutineExerciseSet> getRoutineExercisesSet() {
        return routineExercisesSet;
    }

    public void setRoutineExercisesSet(ArrayList<RoutineExerciseSet> routineExercisesSet) {
        this.routineExercisesSet = routineExercisesSet;
    }

}
