package oyaness.com.exerciseapp.utils;

/**
 * Created by Oya on 18.12.2016.
 */

public interface RemoveRoutineListener {
    void deleteRoutine(long id);
}
