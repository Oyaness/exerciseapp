package oyaness.com.exerciseapp.model;

/**
 * Created by Oya on 15.12.2016.
 */

public class Step {
    private String audio;
    private String image;
    private int stepNb;
    private String text;

    public Step() {
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getStepNb() {
        return stepNb;
    }

    public void setStepNb(int stepNb) {
        this.stepNb = stepNb;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
