package oyaness.com.exerciseapp.utils;

import android.content.Context;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import oyaness.com.exerciseapp.R;
import oyaness.com.exerciseapp.model.ListUserRoutines;

/**
 * Created by Oya on 17.12.2016.
 */

public class JsonUtils {

    private static String readJsonFile(InputStream inputStream) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        byte bufferByte[] = new byte[1024];
        int length;
        try {
            while ((length = inputStream.read(bufferByte)) != -1) {
                outputStream.write(bufferByte, 0, length);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            return null;
        }
        return outputStream.toString();
    }

    public static ListUserRoutines getUserRoutines(Context context) {
        Gson gson = new Gson();
        InputStream inputStream = context.getResources().openRawResource(R.raw.routines_list);
        String jsonString = readJsonFile(inputStream);
        if (jsonString!= null) {
            ListUserRoutines routines = gson.fromJson(jsonString, ListUserRoutines.class);
            return  routines;
        }
        return  null;
    }
}
