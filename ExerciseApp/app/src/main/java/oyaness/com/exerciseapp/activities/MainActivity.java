package oyaness.com.exerciseapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import oyaness.com.exerciseapp.R;
import oyaness.com.exerciseapp.adapters.RoutineAdapter;
import oyaness.com.exerciseapp.model.UserRoutine;
import oyaness.com.exerciseapp.utils.Constants;
import oyaness.com.exerciseapp.utils.JsonUtils;
import oyaness.com.exerciseapp.utils.RemoveRoutineListener;
import oyaness.com.exerciseapp.utils.RoutinesSingleton;

public class MainActivity extends AppCompatActivity implements RemoveRoutineListener{

    private RoutineAdapter adapter;
    private RecyclerView recyclerView;
    private TextView noRoutines;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.rv_routines);
        noRoutines = (TextView) findViewById(R.id.tv_no_routines);

        if (RoutinesSingleton.getInstance().getRoutines() == null){
            RoutinesSingleton.getInstance().setRoutines(JsonUtils.getUserRoutines(this));
        }

        if  (RoutinesSingleton.getInstance().getRoutines()!= null && RoutinesSingleton.getInstance().getRoutines().getListOfUserRoutine()!= null ) {
            adapter = new RoutineAdapter(this, RoutinesSingleton.getInstance().getRoutines().getListOfUserRoutine(), this);
        }
        else {
            adapter = new RoutineAdapter(this, new ArrayList<UserRoutine>(), this);
        }
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        updateViews();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Constants.ROUTINE_INTENT_CODE) {
            long id = data.getLongExtra(Constants.USER_ROUTINE_EXTRA, 0);
            deleteRoutine(id);
        }
    }

    @Override
    public void deleteRoutine(long id) {
        int indexChanged = RoutinesSingleton.getInstance().getRoutines().removeUserRoutineWithId(id);
        adapter.notifyItemRemoved(indexChanged);
        updateViews();
    }

    private void updateViews() {
        if (adapter.getRoutines().size()>0){
            noRoutines.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
        else {
            noRoutines.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }
}
