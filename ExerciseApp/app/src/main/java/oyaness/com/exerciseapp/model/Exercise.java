package oyaness.com.exerciseapp.model;

import java.util.ArrayList;

/**
 * Created by Oya on 15.12.2016.
 */

public class Exercise {
    private String audioFile;
    private String audioMusic;
    private String audioSpeaker;
    private Boolean done;
    private int duration;
    private ArrayList<Equipment> equipmentList;
    //TODO is it?
    private String exerciseCategoryTye;
    private String exerciseDescription;
    private long exerciseId;
    private String exerciseVariationType;
    private Boolean favourite;
    private String goal;
    private String imageUrl;
    //TODO is it?
    private String importId;
    private String name;
    private String prevExercise;
    private ArrayList<Step> stepsList;
    private String subtitle;
    private String summary;
    private Boolean swiped;
    private long topicId;
    private String videoUrl;

    public Exercise() {
    }

    public String getAudioFile() {
        return audioFile;
    }

    public void setAudioFile(String audioFile) {
        this.audioFile = audioFile;
    }

    public String getAudioMusic() {
        return audioMusic;
    }

    public void setAudioMusic(String audioMusic) {
        this.audioMusic = audioMusic;
    }

    public String getAudioSpeaker() {
        return audioSpeaker;
    }

    public void setAudioSpeaker(String audioSpeaker) {
        this.audioSpeaker = audioSpeaker;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getExerciseCategoryTye() {
        return exerciseCategoryTye;
    }

    public void setExerciseCategoryTye(String exerciseCategoryTye) {
        this.exerciseCategoryTye = exerciseCategoryTye;
    }

    public String getExerciseDescription() {
        return exerciseDescription;
    }

    public void setExerciseDescription(String exerciseDescription) {
        this.exerciseDescription = exerciseDescription;
    }

    public long getExerciseId() {
        return exerciseId;
    }

    public void setExerciseId(long exerciseId) {
        this.exerciseId = exerciseId;
    }

    public String getExerciseVariationType() {
        return exerciseVariationType;
    }

    public void setExerciseVariationType(String exerciseVariationType) {
        this.exerciseVariationType = exerciseVariationType;
    }

    public Boolean getFavourite() {
        return favourite;
    }

    public void setFavourite(Boolean favourite) {
        this.favourite = favourite;
    }

    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImportId() {
        return importId;
    }

    public void setImportId(String importId) {
        this.importId = importId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrevExercise() {
        return prevExercise;
    }

    public void setPrevExercise(String prevExercise) {
        this.prevExercise = prevExercise;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Boolean getSwiped() {
        return swiped;
    }

    public void setSwiped(Boolean swiped) {
        this.swiped = swiped;
    }

    public long getTopicId() {
        return topicId;
    }

    public void setTopicId(long topicId) {
        this.topicId = topicId;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public ArrayList<Equipment> getEquipmentList() {
        return equipmentList;
    }

    public void setEquipmentList(ArrayList<Equipment> equipmentList) {
        this.equipmentList = equipmentList;
    }

    public ArrayList<Step> getStepsList() {
        return stepsList;
    }

    public void setStepsList(ArrayList<Step> stepsList) {
        this.stepsList = stepsList;
    }

}
