package oyaness.com.exerciseapp.model;

import java.util.ArrayList;

/**
 * Created by Oya on 15.12.2016.
 */

public class Routine {
    //TODO is it?
    private String author;
    private String description;
    private String duration;
    private ArrayList<Equipment> equipmentsForRoutine;
    //TODO is it?
    private String fitnessLevel;
    //TODO is it?
    private String focusArea;
    private String goal;
    private long id;
    private String imageUrl;
    private String improvementInfo;
    //TODO is it?
    private String intensity;
    private String motivationInfo;
    private String purpose;
    private ArrayList<RoutineExercise> routineExercises;
    private int sizeExercise;
    private String title;
    private String variationType;
    private int variationTypeId;

    public Routine() {
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getFitnessLevel() {
        return fitnessLevel;
    }

    public void setFitnessLevel(String fitnessLevel) {
        this.fitnessLevel = fitnessLevel;
    }

    public String getFocusArea() {
        return focusArea;
    }

    public void setFocusArea(String focusArea) {
        this.focusArea = focusArea;
    }

    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImprovementInfo() {
        return improvementInfo;
    }

    public void setImprovementInfo(String improvementInfo) {
        this.improvementInfo = improvementInfo;
    }

    public String getIntensity() {
        return intensity;
    }

    public void setIntensity(String intensity) {
        this.intensity = intensity;
    }

    public String getMotivationInfo() {
        return motivationInfo;
    }

    public void setMotivationInfo(String motivationInfo) {
        this.motivationInfo = motivationInfo;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public int getSizeExercise() {
        return sizeExercise;
    }

    public void setSizeExercise(int sizeExercise) {
        this.sizeExercise = sizeExercise;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVariationType() {
        return variationType;
    }

    public void setVariationType(String variationType) {
        this.variationType = variationType;
    }

    public int getVariationTypeId() {
        return variationTypeId;
    }

    public void setVariationTypeId(int variationTypeId) {
        this.variationTypeId = variationTypeId;
    }

    public ArrayList<Equipment> getEquipmentsForRoutine() {
        return equipmentsForRoutine;
    }

    public void setEquipmentsForRoutine(ArrayList<Equipment> equipmentsForRoutine) {
        this.equipmentsForRoutine = equipmentsForRoutine;
    }

    public ArrayList<RoutineExercise> getRoutineExercises() {
        return routineExercises;
    }

    public void setRoutineExercises(ArrayList<RoutineExercise> routineExercises) {
        this.routineExercises = routineExercises;
    }

    public int calculateDoneExercises() {
        int sum = 0;
        for (RoutineExercise exercise : routineExercises) {
            if (exercise.getDone()){
                sum++;
            }
        }
        return sum;
    }
}
