package oyaness.com.exerciseapp.model;


import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Oya on 15.12.2016.
 */

public class RoutineExerciseSet {
    private int breaktime;
    private Boolean done;
    private int repeats;
    @SerializedName("setnumber")
    private int setNumber;

    public RoutineExerciseSet() {
    }

    public int getBreaktime() {
        return breaktime;
    }

    public void setBreaktime(int breaktime) {
        this.breaktime = breaktime;
    }


    public int getRepeats() {
        return repeats;
    }

    public void setRepeats(int repeats) {
        this.repeats = repeats;
    }

    public int getSetNumber() {
        return setNumber;
    }

    public void setSetNumber(int setNumber) {
        this.setNumber = setNumber;
    }

    public Calendar getCalendarFromBreaksSec() {
        Calendar cal= GregorianCalendar.getInstance();
        cal.set(0,0,0,0,0,breaktime);
        return cal;
    }
}
