# README #

This is a exercise application. To try it out you can do the following actions:

* Download the project and build it (Don't forget to sync the gradle files) 
* Download and install the APK file.

### Need to know ###

I wasn't sure weather I should change the Json file when the user deletes the routine, so I left it unchanged thinking that in the real application the data would be taken from some service and once a routine is deleted then I would call another deleting service. 

### How it works ###

On the main screen a list of routines is available. By clicking on them, the user can see more details about that routine as well as some exercises that are included in that particular routine.

### Deleting a routine 
A user can delete a routine by clicking on the remove button. The routine is deleted from the list. For purposes of the this application, once the application is closed and started again, all the routines (the deleted ones as well) are in the list.